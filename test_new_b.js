const http = require('http');

const TIMEOUT_IN_MILLISECONDS = 30 * 1000
const NS_PER_SEC = 1e9
const MS_PER_NS = 1e6

var newAgent = new http.Agent({
    keepAlive: true,
    maxSockets: 1,
    keepAliveMsecs: 0
});
const options = {
    agent : newAgent
};

const startBacktest = () => { 
    return new Promise((resolve,reject) => {
        const url = 'http://127.0.0.1:5650/actions/new-backtest/dates/1-05-2018_30-05-2018/identifiers/EURUSD-1M';
        http.get(url,(resp) => {
            
            let data = '';
            resp.on('data', (chunk) => { data += chunk });
            resp.on('end', () => {
                data = JSON.parse(data);
                console.log('data: ',data);
                resolve(data);
            })

        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    });
}

const runBacktest = (backtestConfigInfo) => { 
    return new Promise((resolve,reject) => {
        let i = 0;
        let hasPosition = false;
        let lastValue = '';
        let lastValueCrossName = '';
        const backtestId = backtestConfigInfo.id;
        const backtestPort = backtestConfigInfo.port;
        const origin = `http://127.0.0.1:${backtestPort}/backtests/${backtestId}`;

        const iter = () => {

            let url = origin+'/actions/next';
            if (i === 100) {
                if (hasPosition) {
                    url = origin+'/actions/order/' + lastValueCrossName + '/-2/' + lastValue;
                    hasPosition = false
                } else {
                    url = origin+'/actions/order/' + lastValueCrossName + '/2/' + lastValue;
                    hasPosition = true;
                }
                i=0;
            } else {
                i++;
            }

            http.get(url, options,(resp) => {
                let data = '';
                resp.on('data', (chunk) => { data += chunk });
                resp.on('end', () => {
                    data = JSON.parse(data);
                    switch(data.type) {
                        case 'ORDER':
                            iter();
                        break;
                        case 'QUOTE':
                            lastValue = data.value.close;
                            lastValueCrossName = data.value.identifier;
                            iter();
                        break;
                        case 'RESULTS':
                            const hrend = process.hrtime(hrstart)
                            console.log("finished");
                            console.log('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000);
                            resolve(data);
                        break;
                    }
                })

            }).on("error", (err) => {
                console.log("Error: " + err.message);
            });
        }
        const hrstart = process.hrtime();
        iter();
    });
};

const start = async () => {
    const backtestConfigInfo = await startBacktest();
    const backtestResult = await runBacktest(backtestConfigInfo);
    console.log("BACKTEST RESULT: ",JSON.stringify(backtestResult));
};
start();

